/**
 * @file webcam_receiver.cpp
 * @author Peter Rudolph
 * @brief Simple UDP client receiving/displaying webcam captures from jpeg stream
 */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <boost/asio.hpp>

/** Defines */
// image info
#define WIDTH 320
#define HEIGHT 240
#define CHANNELS 3
// defaults
#define DEFAULT_PORT 9950
#define DEFAULT_SCALE 1.0
// udp
#define MAX_LENGTH 65507

/** Namespaces */
using namespace std;
using namespace cv;
namespace ba = boost::asio;
using ba::ip::udp;

/** Function Headers */
void sigINThandler(int sig);

/** Global variables */
// video writer (recorder)
VideoWriter* g_video_writer;
bool g_recording = false;
  
/**
 * @function main
 */
int main(int argc, char** argv)
{
  // opencv window name
  string window_name = "Webcam Capture - Receiver";
  // arguments
  int port;
  double scale;
  // measurement
  double total_elapsed_time = 0.0;
  double current_elapsed_time = 0.0;
  double elapsed_time = 0.0;
  volatile int fps = 0;
  volatile int size = 0;
  volatile int bytes = 0;
  // timing
  struct timeval start, now;
  long mtime, seconds, useconds;
  // check input args
  if (argc != 1 && argc != 3)
  {
    std::cerr << std::endl
              << "Usage: webcam_receiver <port> <scale>"                         << std::endl
              << "------------------------------------------"                    << std::endl
              << "  port:  port of webcam_sender [" << DEFAULT_PORT << "]"       << std::endl
              << "  scale: scaling of displayed image [" << DEFAULT_SCALE << "]" << std::endl
              << "------------------------------------------"                    << std::endl
              << "  program quits by sending SIGINT (ctrl-c)"                    << std::endl
              << "------------------------------------------"                    << std::endl;
    return 1;
  }
  // default
  if (argc == 1)
  {
    port = DEFAULT_PORT;
    scale = DEFAULT_SCALE;
  }
  else
  {
    port = atoi(argv[1]);
    scale = atof(argv[2]);
  }
  // print input
  std::cout << std::endl
            << "Input Arguments:"   << std::endl
            << "----------------"   << std::endl
            << "  port: "  << port  << std::endl
            << "  scale: " << scale << std::endl;

  // install SIGINT handler
  signal(SIGINT, sigINThandler);
  // boost io service
  boost::asio::io_service io_service;
  // setup udp socket
  udp::socket udp_socket(io_service, udp::endpoint(udp::v4(), port));
  // setup endpoint
  udp::endpoint sender_endpoint(udp::v4(), port);
  // udp raw buffer
  char data[MAX_LENGTH];
  // compressed frame buffer
  cv::vector<uchar> frame_buffer;
  // open window
  namedWindow(window_name, CV_WINDOW_AUTOSIZE);
  for(;;)
  {
    // receive frame
    int data_length = udp_socket.receive_from(ba::buffer(data, MAX_LENGTH), sender_endpoint);
    // resize frame buffer
    frame_buffer.resize(data_length);
    // copy data
    memcpy(&frame_buffer[0], data, data_length);
    // decode frame buffer
    Mat frame = cv::imdecode(frame_buffer, CV_LOAD_IMAGE_COLOR);
    // show image
    if(scale != 1.0)
    {
      Mat scaled_frame(round(frame.rows * scale), round(frame.cols * scale), CV_8UC3);
      resize(frame, scaled_frame, scaled_frame.size(), scale, scale, INTER_LINEAR);
      imshow(window_name, scaled_frame);
    }
    else
      imshow(window_name, frame);
    
    if(g_recording)
      *g_video_writer << frame;
    
    // wait for user input
    int key = waitKey(10);
    if((char)key == 'r')
    {
      if(!g_recording)
      {
        std::cout << "--- start recording ---" << std::endl;
        // create video writer
        g_video_writer = new VideoWriter("webcam_stream.avi", CV_FOURCC('F','F','V','1'), 30, frame.size());
        // timing
        gettimeofday(&start, NULL);
        // update rec state
        g_recording = true;
      }
      else
      {
        std::cout << "--- recording finished ---" << std::endl;
        // remove video writer
        delete g_video_writer;
        // timing
        gettimeofday(&now, NULL);
        seconds  = now.tv_sec  - start.tv_sec;
        useconds = now.tv_usec - start.tv_usec;
        mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
        std::cout << "Length: " << mtime/1000.0 << "s" << std::endl;
        // update rec state
        g_recording = false;
      }
    }
  }
  // cleanup
  udp_socket.close();
  
  return 0;
}

void  sigINThandler(int sig)
{
  char  c;
  signal(sig, SIG_IGN);
  std::cout << "Do you really want to quit? [Y/n] ";
  c = getchar();
  if (c == 'n' || c == 'N')
    signal(SIGINT, sigINThandler);
  else
  {
    if(g_recording)
      delete g_video_writer;
    exit(0);
  }
}

/**
 * @file webcam_sender.cpp
 * @author Peter Rudolph
 * @brief Simple UDP server sending webcam captures as jpeg stream
 */
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <fstream>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <curses.h>

#include <boost/asio.hpp>
#include <boost/timer.hpp>
#include <boost/algorithm/string.hpp>

/** Defines */
// image info
#define WIDTH 320
#define HEIGHT 240
// udp
#define MAX_LENGTH    65507

/** Namespaces */
using namespace std;
using namespace cv;
namespace ba = boost::asio;
using ba::ip::udp;

/** Function Headers */
void quit(char *s);
void sigINThandler(int sig);
void readCPU();
void cleanup();

/** Global variables */
string window_name = "Webcam Capture - Sender";
bool broadcast = false;
// cpu usage
float g_cpu_usage = 0.0;
int g_cpu_used = 0;
int g_cpu_idle = 0;
// ncurses window
WINDOW * mainwin;

/**
 * @function main
 */
int main(int argc, char* argv[])
{
  char * host;
  int port;
  int gray;
  int avgs;
  int row = 0;
  int col = 0;
  int cpu_used_last = 0;
  int cpu_idle_last = 0;
  // read initial cpu usage
  readCPU();
  cpu_used_last = g_cpu_used;
  cpu_idle_last = g_cpu_idle;
  // opencv image capture
  CvCapture* capture;
  // setup camera
  capture = cvCaptureFromCAM( -1 );
  cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH,  WIDTH);
  cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT, HEIGHT);
  //  initialize ncurses
  if ( (mainwin = initscr()) == NULL ) 
  {
    fprintf(stderr, "Error initialising ncurses.\n");
    exit(EXIT_FAILURE);
  }
  //  initialize ncurses colors
  start_color();
  if(has_colors())
  {
    init_pair(1,  COLOR_RED,     COLOR_BLACK);
    init_pair(2,  COLOR_GREEN,   COLOR_BLACK);
    init_pair(3,  COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4,  COLOR_BLUE,    COLOR_BLACK);
    init_pair(5,  COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6,  COLOR_CYAN,    COLOR_BLACK);
    bkgd(COLOR_PAIR(3));
  }
  // install SIGINT handler
  signal(SIGINT, sigINThandler);
  // check input args
  if (argc != 1 && argc != 5)
  {
    bkgd(COLOR_PAIR(1));
    mvaddstr(row++, col, "Usage: webcam_sender <host> <port> <gray> <avgs>");
    mvaddstr(row++, col, "-------------------------------------------");
    mvaddstr(row++, col, "  host:  ip-address of receiver [broadcast]");
    mvaddstr(row++, col, "  port:  port of receiver       [     9950]");
    mvaddstr(row++, col, "  gray:  0 - rgb, 1 - grayscale [        0]");
    mvaddstr(row++, col, "  avgs:  seconds avgs displayed [        5]");
    mvaddstr(row++, col, "-------------------------------------------");
    mvaddstr(row++, col, "  program quits by sending SIGINT (ctrl-c)");
    mvaddstr(row++, col, "-------------------------------------------");
    refresh();
    sleep(3);
    cleanup();
    return 1;
  }
  // default
  if (argc == 1)
  {
    host = "broadcast";
    port = 9950;
    gray = 0;
    avgs = 5;
    broadcast = true;
  }
  else
  {
    host = argv[1];
    port = atoi(argv[2]);
    gray = atoi(argv[3]);
    avgs = atoi(argv[4]);
  }
  // print input
  ostringstream info_stringstream;
  info_stringstream << "Input Arguments:" << endl
                    << "----------------" << endl
                    << "  host: " << host << endl
                    << "  gray: " << gray << endl
                    << "  port: " << port << endl
                    << "  avgs: " << avgs << endl
                    << "----------------" << endl;
  mvaddstr(0, 0, info_stringstream.str().c_str());
  refresh();
  // wait 3 seconds
  sleep(3);
  // clear screen
  clear();
  // prepare output
  ostringstream output_stringstream;
  output_stringstream << "Averages (" << avgs << "s)" << endl
                      << "-------------------"        << endl
                      << "   FPS: "                   << endl
                      << "  size: "                   << endl
                      << "  kB/s: "                   << endl
                      << "  \%CPU: "                  << endl
                      << "-------------------"        << endl;
  mvaddstr(0, 0, output_stringstream.str().c_str());
  refresh();
  // boost error code
  boost::system::error_code error;
  // boost io service
  boost::asio::io_service io_service;
  // get endpoint from input args
  udp::resolver resolver(io_service);
  udp::resolver::query query(udp::v4(), argv[1], argv[2]);
  udp::resolver::iterator endpoint_iterator = resolver.resolve(query);
  // setup broadcast as endpoint
  udp::endpoint sender_endpoint(ba::ip::address_v4::broadcast(), 9950);
  // setup udp socket
  udp::socket udp_socket(io_service);
  udp_socket.open(udp::v4(), error);
  if (!error)
  {
    udp_socket.set_option(ba::ip::udp::socket::reuse_address(true));
    if(broadcast)
      udp_socket.set_option(ba::socket_base::broadcast(true));
  }
  else
    quit("Error while opening UDP socket!");
  // opencv image frame
  Mat frame;
  // compressed frame buffer
  cv::vector<uchar> frame_buffer;
  // start capture
  if( capture )
  {
    // measurement
    double total_elapsed_time = 0.0;
    double current_elapsed_time = 0.0;
    double elapsed_time = 0.0;
    volatile int fps = 0;
    volatile int size = 0;
    volatile int bytes = 0;
    // timing
    struct timeval start, now;
    long mtime, seconds, useconds;
    gettimeofday(&start, NULL);
    // enter capture loop
    for(;;)
    {
      // get frame from capture
      frame = cvQueryFrame( capture );
      if( !frame.empty() )
      {
        // add info to frame
        ostringstream info_stringstream;
        info_stringstream.precision(3);
        info_stringstream << "CPU: " << g_cpu_usage << "%";
        putText(
          frame,                      // image
          info_stringstream.str(),    // text
          cvPoint(30,30),             // place
          FONT_HERSHEY_COMPLEX_SMALL, // font
          0.8,                        // scale
          cvScalar(255,  0,  0),      // color
          1,                          // thickness
          8                           // line type
        );
        
        // de-color image if wanted
        if(gray)
        {
          Mat frame_gray;
          cvtColor(frame, frame_gray, CV_BGR2GRAY);
          frame = frame_gray;
        }
        cv::imencode(".jpg", frame, frame_buffer, std::vector<int>() );
        if(broadcast)
          udp_socket.send_to(ba::buffer(&frame_buffer[0], frame_buffer.size()), sender_endpoint);
        else
          udp_socket.send_to(ba::buffer(&frame_buffer[0], frame_buffer.size()), *endpoint_iterator);
        bytes += frame_buffer.size();
      }
      else
      {
        quit("No captured frame! Exit..."); 
      }
      // measurement
      gettimeofday(&now, NULL);
      seconds  = now.tv_sec  - start.tv_sec;
      useconds = now.tv_usec - start.tv_usec;
      mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
      fps++;
      // print output
      if(mtime >= (avgs * 1000.0))
      {
        // read current cpu usage
        readCPU();
        float cpu_used = g_cpu_used - cpu_used_last;
        float cpu_idle = g_cpu_idle - cpu_idle_last;
        g_cpu_usage = (cpu_used / (cpu_used + cpu_idle)) * 100.0;
        
        cpu_used_last = g_cpu_used;
        cpu_idle_last = g_cpu_idle;
        
        ostringstream fps_stringstream;
        ostringstream size_stringstream;
        ostringstream bytes_stringstream;
        ostringstream cpu_usage_stringstream;
        
        double avg_fps = (double)fps / (double)avgs;
        double avg_bytes = ((double)bytes / (double)avgs) / 1024.0;
        double avg_size = avg_bytes / avg_fps;
        
        fps_stringstream.precision(6);
        size_stringstream.precision(6);
        bytes_stringstream.precision(6);
        cpu_usage_stringstream.precision(6);
        
        fps_stringstream << avg_fps;
        size_stringstream << avg_size;
        bytes_stringstream << avg_bytes;
        cpu_usage_stringstream << g_cpu_usage;
        
        mvaddstr(2, 10, fps_stringstream.str().c_str());
        mvaddstr(3, 10, size_stringstream.str().c_str());
        mvaddstr(4, 10, bytes_stringstream.str().c_str());
        mvaddstr(5, 10, cpu_usage_stringstream.str().c_str());
        refresh();
        
        fps = 0;
        size = 0;
        bytes = 0;
        
        gettimeofday(&start, NULL);
      }
      
    }
  }
  else
    quit("No capture device found!");
  // cleanup 
  cleanup();
  // exit
  return 0;
}

/**
 * @function quit
 *
 * @param message displayed error message
 */
void quit(char *message)
{
  mvaddstr(0,0, message);
  cleanup();
  exit(1);
}

/**
 * @function sigINThandler
 *
 * @param sig signal received
 */
void sigINThandler(int sig)
{
  char  c;
  signal(sig, SIG_IGN);
  clear();
  mvaddstr(0, 0, "Do you really want to quit? [Y/n] ");
  refresh();
  c = getch();
  if (c == 'n' || c == 'N')
    signal(SIGINT, sigINThandler);
  else
  {
    cleanup();
    exit(0);
  }
}

void readCPU()
{
  string line;
  string cpu_str;
  int user_val;
  int niced_val;
  int system_val;
  int idle_val;
  // open /proc/stat for reading
  ifstream proc_stat("/proc/stat");
  if(proc_stat.is_open())
  {
    // get first line
    getline (proc_stat,line);
    // split line
    istringstream iss(line);
    iss >> cpu_str >> user_val >> niced_val >> system_val >> idle_val;
    g_cpu_used = user_val + niced_val + system_val;
    g_cpu_idle = idle_val;
  }
  else
  {
    g_cpu_idle = 0;
    g_cpu_used = 0;
  }
}

void cleanup()
{
  delwin(mainwin);
  endwin();
  refresh();
}

